#!/usr/bin/python3
import json
import os
import sys
from pathlib import Path

import requests
from PyQt5 import QtGui, QtWidgets, QtCore, QtDBus, uic

try:
    import qtpad.gui_search
    from qtpad.preferences import PreferencesDatabase, PreferencesForm
    from qtpad.child import Child
    from qtpad.common import *
except ImportError:
    from preferences import PreferencesDatabase, PreferencesForm
    from child import Child
    from common import *

# Init common settings
LOCAL_DIR, ICONS_DIR, PREFERENCES_FILE, PROFILES_FILE = getStaticPaths()
logger = getLogger()
logger.info("Init of a new instance")
if "qtpad.common" not in sys.modules:
    logger.warning("Could not load pre-compiled modules")


class Mother(object):
    def __init__(self):
        # Load preferences
        self.preferencesIndexes = {"menu": 0, "css": None}
        self.preferences = PreferencesDatabase()
        self.loadCss()
        self.clipboard = app.clipboard()

        # Load icons
        self.icon = {}
        for icon in os.listdir(ICONS_DIR):
            iconName = os.path.splitext(icon)
            if iconName[1] == ".svg":
                self.icon[iconName[0]] = QtGui.QIcon(ICONS_DIR + icon)

        # Init search form
        self.lastActive = ""
        self.searchForm = SearchForm(self)

        # Load notes
        if not self._notesDir().exists():
            self._notesDir().mkdir(parents=True)

        configPath = Path("~/.config/qtpad/").expanduser()
        if not configPath.exists():
            configPath.mkdir(parents=True)

        if self.preferences.query("general", "deleteEmptyNotes"):
            self._cleanEmptyNotes()
        self._cleanProfiles()
        self._cleanEnabled()
        self.children = {}
        self._enableNotesWithoutFolder()
        self.updateNotes()

        # Init system tray icon
        # Handle svg to pixmap conversion (KDE compatibility)
        trayIcon = self.icon["tray"].pixmap(64, 64)
        trayIcon = QtGui.QIcon(trayIcon)
        self.menu = QtWidgets.QMenu()
        self.menu.aboutToShow.connect(self._menuRefresh)
        self.trayIcon = QtWidgets.QSystemTrayIcon()
        self.trayIcon.activated.connect(self.clickEvent)
        self.trayIcon.setIcon(trayIcon)
        self._menuRefresh()
        self.trayIcon.setContextMenu(self.menu)
        self.trayIcon.show()

        # Init startup action
        action = self.preferences.query("actions", "events", "startup")
        command = self.preferences.query("exec", "events", "startup")
        self.noteAction(action, command)

    def clickEvent(self, event):
        if event == QtWidgets.QSystemTrayIcon.Trigger:
            action = self.preferences.query(
                "actions", "tray icon", "left click")
            command = self.preferences.query("exec", "tray icon", "left click")
            self.noteAction(action, command)

        elif event == QtWidgets.QSystemTrayIcon.MiddleClick:
            action = self.preferences.query(
                "actions", "tray icon", "middle click")
            command = self.preferences.query(
                "exec", "tray icon", "middle click")
            self.noteAction(action, command)

    def _clipboardFetch(self, newNote=False):
        pixmap = self.clipboard.pixmap()
        path = self.clipboard.text().rstrip()
        textContent = None
        if pixmap.isNull():
            if os.path.isfile(path) and os.stat(path).st_size > 0:
                allowed = ["txt", "gif", "png", "bmp", "jpg", "jpeg", "svg"]
                ext = os.path.splitext(path.lower())[1][1:]
                if ext in allowed:
                    if self.preferences.query("general", "fetchTxt") and ext == "txt":
                        with open(path) as f:
                            textContent = f.read()
                    elif self.preferences.query("general", "fetchFile"):
                        pixmap = QtGui.QPixmap(path)

            elif self.preferences.query("general", "fetchUrl") and (path.startswith("http://") or path.startswith("https://") or path.startswith("www.")):
                if path.lower().find(".pdf") == -1:
                    allowed = ['jpeg', 'gif', 'png', 'bmp', 'svg+xml']
                    try:
                        mimetype = requests.get(
                            path).headers["content-type"].split('/')[1]
                        if mimetype in allowed:
                            fetch = requests.get(path)
                            pixmap.loadFromData(fetch.content)
                    except Exception:
                        logger.warning(
                            str(sys.exc_info()[0]), str(sys.exc_info()[1]))

        if textContent or not pixmap.isNull():
            if newNote:
                if textContent:
                    self.children[self.createNote(text=textContent)]
                    logger.info(f"Fetched text from {path}")
                else:
                    self.children[self.createNote(image=pixmap)]
                    logger.info("Fetched image from " +
                                (path if path else "clipboard"))

                if self.preferences.query("general", "fetchClear"):
                    self.clipboard.setText("")
                    logger.warning("Emptied clipboard content")
            return True
        return False

    def _cmdParse(self, cmd):
        logger.info(f"Call from command line interface: {cmd}")
        actions = []
        if "action" in cmd:
            actions += cmd["action"]
        if "a" in cmd:
            actions += cmd["a"]
        for action in actions:
            self.noteAction(action)

    def _folderRenamePrompt(self, folder):
        msg = QtWidgets.QInputDialog()
        msg.setInputMode(QtWidgets.QInputDialog.TextInput)
        msg.setWindowFlags(msg.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        msg.setWindowTitle(f"Rename '{folder}'")
        msg.setLabelText("Enter the new name:")
        msg.setTextValue(str(Path(folder).relative_to(self._notesDir())))
        msg.setFixedSize(250, 100)
        accept = msg.exec_()
        newName = msg.textValue()
        newName = sanitizeString(newName)
        newPath = self._notesDir() / newName
        if accept and newName and not newPath.exists():
            self._renameFolder(folder, newName)

    def _menuAddOption(self, option):
        option = option.lower()
        icon = self.icon.get(option.replace(" ", "_"))

        if option == "(separator)":
            self.menu.addSeparator()

        elif option == "preferences":
            self.menu.addAction(icon, "Preferences",
                                lambda: PreferencesForm(self))

        elif option == "quit":
            self.menu.addAction(icon, 'Quit', app.exit)

        elif option == "folders list":
            self._menuFolders()

        elif option == "notes list":
            self._menuListNotes()

        elif option == "fetch clipboard":
            if self._clipboardFetch():
                self.menu.addAction(icon, option.capitalize(),
                                    lambda: self.noteAction(option))

        else:
            self.menu.addAction(icon, option.capitalize(),
                                lambda: self.noteAction(option))

    def _menuFolders(self):

        # without references the gui-elements would get removed
        self.folderMenus = []
        self.folderActions = []

        for folder in map(Path, self.folders()):
            if len(list(folder.iterdir())) == 0:
                continue

            menu = QtWidgets.QMenu()
            self.folderMenus.append(menu)

            numEnabledNotes = 0
            for total, path in enumerate(sorted(list(folder.iterdir()))):
                note = self._fullname(path)

                action = QtWidgets.QAction(path.name, checkable=True)
                self.folderActions.append(action)

                if note in self.children:
                    numEnabledNotes += 1
                    action.triggered.connect(
                        lambda _, note=note: self.disableNote(note))
                    action.setChecked(True)
                else:
                    action.triggered.connect(
                        lambda _, note=note: self.enableNote(note))
                menu.addAction(action)

            menu.setTitle(
                f"{folder.relative_to(self._notesDir())} ({numEnabledNotes}/{total+1})")
            if numEnabledNotes > 0:
                menu.setIcon(self.icon["folder_active"])
            else:
                menu.setIcon(self.icon["folder_inactive"])
            menu.addSeparator()
            menu.addAction(self.icon["load_all_folders"], "Load all",
                           lambda folder=folder: self.enableFolder(str(folder)))
            menu.addAction(self.icon["unload_all_folders"], "Unload all",
                           lambda folder=folder: self.disableFolder(str(folder)))
            menu.addAction(self.icon["rename"], "Rename folder",
                           lambda folder=folder: self._folderRenamePrompt(str(folder)))
            menu.addAction(self.icon["delete"], "Delete folder",
                           lambda folder=folder: self._deleteFolder(str(folder)))
            self.menu.addMenu(menu)

    def _menuListNotes(self):
        notes = []

        # Sort children list, add notes without folder first
        for name in sorted(self.children, key=str.lower):
            if "/" not in name:
                notes.append(name)

        for name in sorted(self.children, key=str.lower):
            if "/" in name:
                notes.append(name)

        # Define the appropriate icon
        for name in notes:
            if self.children[name].profile.query("pin"):
                icon = self.icon["file_pinned"]
            elif name in self.preferences.query("actives"):
                icon = self.icon["file_active"]
            elif self.children[name].extension == ".png":
                icon = self.icon["file_image"]
            else:
                icon = self.icon["file_inactive"]
            self.menu.addAction(
                icon, self.children[name].fullname, self.children[name].noteDisplay)

    def _menuRefresh(self):
        # Monitor changes in the database directory
        self.updateNotes()
        self._cleanOprhanNotes()
        self._cleanProfiles()

        # Generate dynamic menu
        self.menu.clear()
        for option in self.preferences.query("menus", "mother"):
            self._menuAddOption(option)

    def _cleanEmptyNotes(self):
        for f in self._notesDir().iterdir():
            if f.is_dir():
                for note in f.iterdir():
                    if note.stat().st_size == 0:
                        note.unlink()
                        logger.warning(f"Removed '{note}' (empty)")
            else:
                if f.stat().st_size == 0:
                    f.unlink()
                    logger.warning(f"Removed '{f}' (empty)")

    def _notes(self):
        notesDir = Path(self.preferences.query("general", "notesDb"))
        for f in notesDir.iterdir():
            if (notesDir / f).is_dir():
                for note in (notesDir / f).iterdir():
                    if self._valid_note_extension(notesDir + f):
                        return self._fullname(notesDir / f / note)
            else:
                if self._valid_note_extension(notesDir / f):
                    return self._fullname(f)

    def _valid_note_extension(self, path):
        return Path(path).suffix in [".txt", ".png"]
                    
    def _cleanOprhanNotes(self):
        for f in list(self.children):
            if not Path(self.children[f].path).exists():
                logger.warning(
                    f"Removed '{self.children[f].fullname}' (orphan)")
                self.children[f].noteDelete()

    def _cleanProfiles(self):
        if Path(PROFILES_FILE).is_file():
            with open(PROFILES_FILE, "r+") as db:
                profiles = json.load(db)
                for note in list(profiles):
                    if not self._matchingNotePath(note):
                        del profiles[note]
                        logger.warning(f"Removed '{note}' (profile)")
                db.seek(0)
                db.truncate()
                db.write(json.dumps(profiles, indent=2, sort_keys=False))

    def _cleanEnabled(self):
        for note in self.preferences.db["enabled"]:
            if not self._matchingNotePath(note):
                self.preferences.db["enabled"].remove(note)
                logger.warning(f"Removed '{note}' (enabled)")
        self.preferences.save()

    def loadCss(self):
        configPath = os.path.expanduser("~/.config/qtpad/")
        stylesheet = ""
        for f in os.listdir(configPath):
            if os.path.splitext(f)[1] == ".css":
                with open(configPath + f) as f:
                    stylesheet += f.read() + "\n"
        app.setStyleSheet(stylesheet)

    def folders(self):
        folders = []
        for folder in self._notesDir().iterdir():
            if folder.name != ".trash" and folder.is_dir():
                folders.append(folder)
        return map(str, sorted(folders, key=lambda p: str(p).lower()))

    def _notes(self):
        result = []
        for f in self._notesDir().iterdir():
            if f.is_dir():
                for note in f.iterdir():
                    if self._isValidNoteExtension(f / note):
                        result.append(self._fullname(f / note))
            else:
                if self._isValidNoteExtension(f):
                    result.append(self._fullname(f))
        return result

    def _notesDir(self):
        return Path(self.preferences.query("general", "notesDb"))

    def _isValidNoteExtension(self, path):
        return Path(path).suffix in [".txt", ".png"]

    def _matchingNotePath(self, fullname):
        assert isinstance(fullname, str)

        types = ['.txt', '.png']
        for type in types:
            path = self._notesDir() / (fullname + type)
            if path.exists():
                return path
        return None

    def noteAction(self, action, cmd=None):
        action = action.lower()
        if action == "none":
            pass

        elif action == "new note":
            self.createNote()

        elif action == "load all folders":
            self.updateNotes()

        elif action == "unload all folders":
            self.disableAllFolders()

        elif action == "fetch clipboard":
            fetch = self._clipboardFetch(newNote=True)

        elif action == "fetch clipboard or new note":
            fetch = self._clipboardFetch(newNote=True)
            if not fetch:
                self.createNote()

        elif action == "toggle actives":
            actives = []
            for name in self.children:
                child = self.children[name]
                if child.profile.query("pin"):
                    child.activateWindow()
                elif child.isVisible():
                    actives.append(child.fullname)

            if actives:
                self.preferences.set("actives", actives)
                self.noteAction("hide all")
            elif self.preferences.query("actives"):
                for note in self.preferences.query("actives"):
                    if note in self.children:
                        self.children[note].noteDisplay()

        elif action == "hide all":
            for name in list(self.children):
                child = self.children[name]
                if child.isVisible() and not child.profile.query("pin"):
                    child.close()

        elif action == "show all":
            for name in self.children:
                if not self.children[name].isVisible():
                    self.children[name].noteDisplay()

        elif action == "reverse all":
            for name in list(self.children):
                child = self.children[name]
                if child.isVisible():
                    if not child.profile.query("pin"):
                        child.close()
                else:
                    child.noteDisplay()

        elif action == "reset positions":
            n = 0
            _x = QtWidgets.QDesktopWidget().screenGeometry().width() - \
                self.preferences.query("styleDefault", "width")
            _y = self.preferences.query("styleDefault", "height") / 2
            width = self.preferences.query("styleDefault", "width")
            height = self.preferences.query("styleDefault", "height")
            for name in self.children:
                child = self.children[name]
                n += 28
                x = _x - n
                y = _y + n
                child.noteDisplay()
                child.setGeometry(x, y, width, height)
                child.profile.load()
                child.profile.set("x", x)
                child.profile.set("y", y)
                child.profile.set("width", width)
                child.profile.set("height", height)
                child.profile.save()

        elif action == "exec":
            if cmd:
                logger.info(f"Starting process '{cmd}'")
                slave = QtCore.QProcess()
                slave.startDetached(cmd)

        else:
            logger.error(f"Invalid main menu option '{action}'")

    def createNote(self, image=None, text=None):
        if image:
            prefix = self.preferences.query("general", "nameImage")
        else:
            prefix = self.preferences.query("general", "nameText")

        name = getNameIndex(prefix, self.children)
        if image:
            self.children[name] = Child(
                self, str(self._notesDir() / (name + ".eng")), popup=True, image=image)
        else:
            self.children[name] = Child(
                self, str(self._notesDir() / (name + ".txt")), popup=True, text=text)

        self.enableNote(name)

        return name

    def enableNote(self, fullname):
        assert isinstance(fullname, str)

        if fullname not in self.preferences.db["enabled"]:
            self.preferences.db["enabled"].append(fullname)
            self.preferences.save()
        self.updateNotes()

    def disableNote(self, fullname):
        assert isinstance(fullname, str)

        if fullname in self.preferences.db["enabled"]:
            self.preferences.db["enabled"].remove(fullname)
            self.preferences.save()
        self.updateNotes()

    def isNoteEnabled(self, fullname):
        assert isinstance(fullname, str)

        return fullname in self.preferences.db["enabled"]

    def enableFolder(self, folder):
        for f in (self._notesDir() / folder).iterdir():
            if self._isValidNoteExtension(f):
                self.enableNote(self._fullname(f))
        self.updateNotes()

    def disableFolder(self, folder):
        self.preferences.db["enabled"] = [
            note for note in self.preferences.db["enabled"]
            if Path(folder) not in (self._notesDir() / note).parents
        ]
        self.preferences.save()
        self.updateNotes()

    def disableAllFolders(self):
        for child in list(self.children.values()):
            if child.folder:
                self.disableNote(child.fullname)
        self.updateNotes()

    def _enableNotesWithoutFolder(self):
        self.enableFolder(self._notesDir())

    def updateNotes(self):

        # load notes that became enabled
        for note in set(self.preferences.db["enabled"]) - set(self.children):
            self._loadNote(note)

        # unload notes that became disabled and remove them from self.children
        for note in set(self.children) - set(self.preferences.db["enabled"]):
            self._unloadNote(note)
            del self.children[note]

    def _loadNote(self, fullname, popup=True):
        assert isinstance(fullname, str)

        path = os.path.join(self.preferences.query(
            "general", "notesDb"), fullname)
        if fullname not in self.children:
            path_with_ext = path + ".txt"
            if os.path.exists(path_with_ext):
                self.children[fullname] = Child(self, path_with_ext, popup)
                return

            path_with_ext = path + ".png"
            if os.path.exists(path_with_ext):
                self.children[fullname] = Child(
                    self, path, image=QtGui.QPixmap(path_with_ext), popup=popup)

            raise RuntimeError(
                f"Expected to find a note at {path} (.txt or .png)")

    def _unloadNote(self, fullname):
        assert isinstance(fullname, str)

        self.children[fullname].noteClose()

    def _deleteFolder(self, folder):
        folder = self._notesDir() / folder
        if len(list(folder.iterdir())) == 0:
            folder.rmdir()
            logger.warning(f"Removed empty folder '{folder}' (user)")
        else:
            msg = QtWidgets.QMessageBox()
            msg.setWindowFlags(msg.windowFlags() |
                               QtCore.Qt.WindowStaysOnTopHint)
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Delete confirmation")
            msg.setText("Please confirm deletion of folder\n" +
                        str(folder.relative_to(self._notesDir())))
            msg.setStandardButtons(
                QtWidgets.QMessageBox.Apply | QtWidgets.QMessageBox.Cancel)
            if msg.exec_() == QtWidgets.QMessageBox.Apply:
                for f in folder.iterdir():
                    f.unlink()
                folder.rmdir()
                logger.warning(f"Removed folder '{folder}' (user)")
                self._cleanOprhanNotes()
            self.updateNotes()

    def _renameFolder(self, oldName, newName):
        try:
            (self._notesDir() / oldName).rename(self._notesDir() / newName)
        except OSError:
            logger.error("Could not rename folder")
        except FileNotFoundError:
            logger.error(f"Folder not found ({oldName})")
        else:
            # Rename loaded notes
            for child in self.children:
                if self.children[child].fullname.startswith(oldName + "/"):
                    self.children[child].noteMove(newName, soft=True)

            # Rename folders in unloaded list
            self.disableFolder(oldName)
            self.enableFolder(newName)

            logger.info(f"Renamed folder '{oldName}' to '{newName}'")
            self.updateNotes()

    def _fullname(self, path):
        return Child.toFullname(str(path), self.preferences.query("general", "notesDb"))


class SearchForm(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__()

        # Load the ui file in case the gui modules are not loaded
        if "qtpad.gui_search" in sys.modules:
            self.ui = qtpad.gui_search.Ui_Form()
            self.ui.setupUi(self)
        else:
            self.ui = uic.loadUi(LOCAL_DIR + 'gui_search.ui', self)

        self.parent = parent
        self.active = None
        self.setFixedSize(490, 100)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setWindowFlags(self.windowFlags() |
                            QtCore.Qt.WindowStaysOnTopHint)
        self.installEventFilter(self)
        self.ui.searchFindButton.clicked.connect(self._find)
        self.ui.searchFindAllButton.clicked.connect(self._findAll)
        self.ui.searchReplaceButton.clicked.connect(self._replace)
        self.ui.searchReplaceAllButton.clicked.connect(self._replaceAll)

    def eventFilter(self, object, event):
        eventType = event.type()
        if eventType == QtCore.QEvent.Show:
            self._updateTitle()
            self.ui.searchFindLine.setText(
                self.active.ui.textEdit.textCursor().selectedText())
        elif eventType == QtCore.QEvent.FocusIn or eventType == QtCore.QEvent.Enter:
            self._updateTitle()
        return QtCore.QObject.event(object, event)

    def _find(self):
        next = self.active.ui.textEdit.find(
            self.ui.searchFindLine.text(), self._getFlag())
        if not next and self.ui.searchWrapBox.isChecked():
            self.active.ui.textEdit.moveCursor(QtGui.QTextCursor.Start)
            self.active.ui.textEdit.find(
                self.ui.searchFindLine.text(), self._getFlag())

    def _findAll(self, find=""):
        # Allow override or searched text
        if not find:
            find = self.ui.searchFindLine.text()

        # Remove previous underlines
        extraSelections = self.active.ui.textEdit.extraSelections()
        for extra in extraSelections:
            extra.format.setFontUnderline(False)

        # Underline all results
        self.active.ui.textEdit.moveCursor(QtGui.QTextCursor.Start)
        while self.active.ui.textEdit.find(find, self._getFlag()):
            extra = QtWidgets.QTextEdit.ExtraSelection()
            extra.cursor = self.active.ui.textEdit.textCursor()
            extra.format.setFontUnderline(True)
            extraSelections.append(extra)

        # Apply result and clear selection
        self.active.ui.textEdit.setExtraSelections(extraSelections)
        self.active.ui.textEdit.moveCursor(QtGui.QTextCursor.Start)

    def _getFlag(self):
        flag = QtGui.QTextDocument.FindFlags(0)
        if self.ui.searchCaseBox.isChecked():
            flag = flag | QtGui.QTextDocument.FindCaseSensitively
        if self.ui.searchWholeBox.isChecked():
            flag = flag | QtGui.QTextDocument.FindWholeWords
        return flag

    def _replace(self):
        find = self.ui.searchFindLine.text()
        replace = self.ui.searchReplaceLine.text()
        cursor = self.active.ui.textEdit.textCursor()
        if find.lower() == cursor.selectedText().lower():
            cursor.insertText(replace)
            self.active.noteTextSave()
        self._find()

    def _replaceAll(self):
        find = self.ui.searchFindLine.text()
        replace = self.ui.searchReplaceLine.text()
        extraSelections = self.active.ui.textEdit.extraSelections()
        cursor = self.active.ui.textEdit.textCursor()

        self.active.ui.textEdit.moveCursor(QtGui.QTextCursor.Start)
        while self.active.ui.textEdit.find(find, self._getFlag()):
            extra = QtWidgets.QTextEdit.ExtraSelection()
            extra.cursor = self.active.ui.textEdit.textCursor()
            end = extra.cursor.position()
            start = end - len(find)
            cursor.setPosition(start)
            cursor.setPosition(end, QtGui.QTextCursor.KeepAnchor)
            cursor.insertText(replace)
            extraSelections.append(extra)
        self.active.ui.textEdit.setExtraSelections(extraSelections)
        self.active.ui.textEdit.moveCursor(QtGui.QTextCursor.Start)
        self._findAll(replace)  # Underline replaced text
        self.active.noteTextSave()

    def _updateActive(self):
        for name in self.parent.children:
            child = self.parent.children[name]
            if child.isVisible() and child.extension == ".txt":
                return child
        return None

    def _updateTitle(self):
        # Fallback to another opened note if last active is deleted or closed
        if not self.parent.lastActive.fullname:
            self.active = self._updateActive()
        elif not self.parent.lastActive.isVisible():
            self.active = self._updateActive()
        else:
            self.active = self.parent.lastActive

        # Hide orphan search form
        if self.active is None:
            self.hide()
        else:
            self.setWindowTitle(f"Search in '{self.active.fullname}'")



class QDBusServer(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.__dbusAdaptor = QDBusServerAdapter(self)


class QDBusServerAdapter(QtDBus.QDBusAbstractAdaptor):
    QtCore.Q_CLASSINFO("D-Bus Interface", "org.qtpad.session")
    QtCore.Q_CLASSINFO("D-Bus Introspection",
                       '  <interface name="org.qtpad.session">\n'
                       '    <method name="parse">\n'
                       '      <arg direction="in" type="s" name="cmd"/>\n'
                       '    </method>\n'
                       '  </interface>\n')

    def __init__(self, parent):
        super().__init__(parent)

    @QtCore.pyqtSlot(str)
    def parse(self, cmd):
        if cmd:
            # Serialize the string of commands into a dictionnary
            current = ""
            commands = {}
            for arg in cmd.split("%"):
                if arg.startswith("-"):
                    current = arg.lstrip("-")
                elif current:
                    commands[current].append(arg)
                if current not in commands:
                    commands[current] = []

            # Pass the commands object to the main parser
            mother._cmdParse(commands)


def main(cmd=None, arg=None):
    global app, mother
    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    bus = QtDBus.QDBusConnection.sessionBus()
    server = QDBusServer()
    bus.registerObject('/org/qtpad/session', server)
    bus.registerService('org.qtpad.session')
    mother = Mother()

    # Handle init command (if any)
    if cmd and arg:
        mother.parse(cmd, arg)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
